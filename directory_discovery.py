import argparse
import requests
from concurrent.futures import ProcessPoolExecutor

def check_dir(url, directory):
    full_url = f"{url}/{directory}"
    response = requests.get(full_url)
    if response.status_code != 404:
        print(f"[{response.status_code}] Found: {full_url}")

def main():
    parser = argparse.ArgumentParser(description="Directory discovery script")
    parser.add_argument("-u", "--url", required=True, help="Target URL")
    parser.add_argument("-w", "--wordlist", required=True, help="Path to the wordlist file")

    args = parser.parse_args()

    with open(args.wordlist, "r") as f:
        directories = [line.strip() for line in f.readlines()]

    print(f"Starting directory discovery on {args.url} using {len(directories)} directories...")

    with ProcessPoolExecutor() as executor:
        for directory in directories:
            executor.submit(check_dir, args.url, directory)

    print("Directory discovery complete.")

if __name__ == "__main__":
    main()
