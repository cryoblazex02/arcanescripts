require 'base32'
require 'base64'
require 'ascii85'
require 'net/http'
require 'openssl'

def b32_decode(cipher)
  Base32.decode(cipher)
end

def b64_decode(cipher)
  Base64.decode64(cipher)
end

def b85_decode(cipher)
  Ascii85.decode(cipher)
end

def bitotext(cipher)
  cipher.to_i(2).chr
end

def octtotext(cipher)
  cipher.to_i(8).chr
end

def dectotext(cipher)
  cipher.to_i(10).chr
end

def hextotext(cipher)
  [cipher].pack('H*')
end

def rot47_decode(cipher)
  cipher.tr('!-~', 'P-~!-O')
end

def get_user_input(prompt)
  print prompt
  gets.to_i
end

def weinerrsa_decode(n, e, c)   
  def rat_to_cont_frac(x, y)
    a = x / y
    p_quotient = [a]
    while a * y != x
      x, y = y, x - a * y
      a = x / y
      p_quotient.append(a)
    end
    p_quotient
  end

  def cont_frac_to_conts(frac)
    conts = []
    (0...frac.length).each do |i|
      conts.append(cont_frac_to_rat(frac[0..i]))
    end
    conts
  end

  def cont_frac_to_rat(frac)
    return [0, 1] if frac.empty?

    num = frac.last
    denom = 1
    (-2..-frac.length).step(-1).each do |i|
      num, denom = frac[i] * num + denom, num
    end
    [num, denom]
  end

  def extended_gcd(a, b)
    return [b, 0, 1] if a == 0

    g, x, y = extended_gcd(b % a, a)
    [g, y - (b / a) * x, x]
  end

  def modular_inverse(a, m)
    g, x, _ = extended_gcd(a, m)
    return nil if g != 1

    (x + m) % m
  end

  def integer_sqrt(n)
    x = n
    y = (x + 1) / 2
    while y < x
      x = y
      y = (x + n / x) / 2
    end
    x
  end

  def crack_rsa(e, n)
    frac = rat_to_cont_frac(e, n)
    convergents = cont_frac_to_conts(frac)
    convergents.each do |k, d|
      next unless k != 0 && (e * d - 1) % k == 0

      phi = (e * d - 1) / k
      s = n - phi + 1
      d_value = s * s - 4 * n
      next unless d_value >= 0

      sq = integer_sqrt(d_value)
      return d if sq * sq == d_value && (s + sq) % 2 == 0
    end
    nil
  end

  begin
    d = crack_rsa(e, n)

    unless d
      puts "Wiener's attack failed: Unable to find private exponent."
      return
    end

    m = (c**d) % n
    flag = [m].pack('Q>').force_encoding('UTF-8')

    unless flag.valid_encoding?
      puts "Error: Decrypted message contains invalid UTF-8 encoding."
      return
    end

    puts "Decrypted Message: #{flag}"
  rescue StandardError => e
    puts "An error occurred: #{e.message}"
  end
end

def commonmodulus_decode(n1, n2, n3, c)
  # Assuming a common exponent (e.g., e = 65537)
  e = 65537
  c1 = c.to_bn.mod_exp(e, n1).to_i
  c2 = c.to_bn.mod_exp(e, n2).to_i
  c3 = c.to_bn.mod_exp(e, n3).to_i

  result = chinese_remainder_theorem([n1, n2, n3], [c1, c2, c3])
  result.to_s
end

def classicrsa_decode(n, e, c)           
  url_1 = "http://factordb.com/index.php?query=%i"
  url_2 = "http://factordb.com/index.php?id=%s"

  ids = Net::HTTP.get(URI.parse(url_1 % n)).scan(/index\.php\?id\=([0-9]+)/i)[1..-1]

  factors = ids.map { |fact_id| Net::HTTP.get(URI.parse(url_2 % fact_id)).scan(/value=\"([0-9\^\-]+)\"/i).first[0].to_i - 1 }

  puts "[+] Multiple Prime Factors Found." if factors.length > 2

  phi = factors.reduce(:*)
  d = OpenSSL::BN.new(e).mod_inverse(phi)
  m = OpenSSL::BN.new(c).mod_exp(d, n).to_i

  puts "[+] Decoded Text: #{[m].pack('Q*')}"
end

def chinese_remainder_theorem(moduli, values) 
  product = moduli.reduce(:*)
  moduli.zip(values).sum { |mod, value| value * product / mod * OpenSSL::BN.new(product / mod).mod_inverse(mod) } % product
end

def brainfuck_decode(code) 
  tape, p, output, c = [0]*30_000, 0, "", 0
  while c < code.size
    case code[c]
    when ?+ then tape[p] = (tape[p] + 1) % 256
    when ?- then tape[p] = (tape[p] - 1) % 256
    when ?> then p = (p + 1) % 30_000
    when ?< then p = (p - 1) % 30_000
    when ?. then output += tape[p].chr
    when ?, then # Input operation (not implemented in this simple example)
    when ?[ then c = tape[p].zero? ? jump_forward(c, code) : c
    when ?] then c = tape[p].zero? ? c : jump_backward(c, code)
    end
    c += 1
  end
  output
end

def jump_forward(c, code)
  loop_count = 1
  while loop_count > 0
    c += 1
    loop_count += 1 if code[c] == ?[
    loop_count -= 1 if code[c] == ?]
  end
  c
end

def jump_backward(c, code)
  loop_count = 1
  while loop_count > 0
    c -= 1
    loop_count += 1 if code[c] == ?]
    loop_count -= 1 if code[c] == ?[
  end
  c
end

def bacon26_decode(code)  
  bacon_alphabet = {
    'AAAAA' => 'A', 'AAAAB' => 'B', 'AAABA' => 'C', 'AAABB' => 'D', 'AABAA' => 'E',
    'AABAB' => 'F', 'AABBA' => 'G', 'AABBB' => 'H', 'ABAAA' => 'I', 'ABAAB' => 'J',
    'ABABA' => 'K', 'ABABB' => 'L', 'ABBAA' => 'M', 'ABBAB' => 'N', 'ABBBA' => 'O',
    'ABBBB' => 'P', 'BAAAA' => 'Q', 'BAAAB' => 'R', 'BAABA' => 'S', 'BAABB' => 'T',
    'BABAA' => 'U', 'BABAB' => 'V', 'BABBA' => 'W', 'BABBB' => 'X', 'BBAAA' => 'Y',
    'BBAAB' => 'Z'
  }

  # Remove spaces from encoded_text
  code = code.gsub(/\s+/, '')

  groups_of_five = code.scan(/.{5}/)
  decoded_text = groups_of_five.map { |group| bacon_alphabet[group] }.join

  decoded_text
end

def bacon24_decode(code) 
  bacon_alphabet = {
    'AAAAA' => 'A', 'AAAAB' => 'B', 'AAABA' => 'C', 'AAABB' => 'D', 'AABAA' => 'E',
    'AABAB' => 'F', 'AABBA' => 'G', 'AABBB' => 'H', 'ABAAA' => 'I', 'ABAAB' => 'K',
    'ABABA' => 'L', 'ABABB' => 'M', 'ABBAA' => 'N', 'ABBAB' => 'O', 'ABBBA' => 'P',
    'ABBBB' => 'Q', 'BAAAA' => 'R', 'BAAAB' => 'S', 'BAABA' => 'T', 'BAABB' => 'U',
    'BABAA' => 'W', 'BABAB' => 'X', 'BABBA' => 'Y', 'BABBB' => 'Z'
  }

  # Remove spaces from encoded_text
  code = code.gsub(/\s+/, '')

  groups_of_five = code.scan(/.{5}/)
  decoded_text = groups_of_five.map { |group| bacon_alphabet[group] || group }.join

  decoded_text
end

MORSE_CODE_SET = {
  ".-" => "A", "-..." => "B", "-.-." => "C", "-.." => "D", "." => "E",
  "..-." => "F", "--." => "G", "...." => "H", ".." => "I", ".---" => "J",
  "-.-" => "K", ".-.." => "L", "--" => "M", "-." => "N", "---" => "O",
  ".--." => "P", "--.-" => "Q", ".-." => "R", "..." => "S", "-" => "T",
  "..-" => "U", "...-" => "V", ".--" => "W", "-..-" => "X", "-.--" => "Y",
  "--.." => "Z", "-----" => "0", ".----" => "1", "..---" => "2", "...--" => "3",
  "....-" => "4", "....." => "5", "-...." => "6", "--..." => "7", "---.." => "8",
  "----." => "9"
}.freeze

def morsecode_decode(code)   
  words = code.split("\n")

  decoded_message = words.map do |word|
    letters = word.split(" ")

    decoded_word = letters.map { |letter| MORSE_CODE_SET[letter] || " " }.join

    decoded_word
  end.join(" ")
end

CODON_TABLE = {
  'AAA' => 'a', 'AAC' => 'b', 'AAG' => 'c', 'AAT' => 'd', 
  'ACA' => 'e', 'ACC' => 'f', 'ACG' => 'g', 'ACT' => 'h',
  'AGA' => 'i', 'AGC' => 'j', 'AGG' => 'k', 'AGT' => 'l',
  'ATA' => 'm', 'ATC' => 'n', 'ATG' => 'o', 'ATT' => 'p',
  'CAA' => 'q', 'CAC' => 'r', 'CAG' => 's', 'CAT' => 't',
  'CCA' => 'u', 'CCC' => 'v', 'CCG' => 'w', 'CCT' => 'x',
  'CGA' => 'y', 'CGC' => 'z', 'CGG' => 'A', 'CGT' => 'B',
  'CTA' => 'C', 'CTC' => 'D', 'CTG' => 'E', 'CTT' => 'F',
  'GAA' => 'G', 'GAC' => 'H', 'GAG' => 'I', 'GAT' => 'J',
  'GCA' => 'K', 'GCC' => 'L', 'GCG' => 'M', 'GCT' => 'N',
  'GGA' => 'O', 'GGC' => 'P', 'GGG' => 'Q', 'GGT' => 'R',
  'GTA' => 'S', 'GTC' => 'T', 'GTG' => 'U', 'GTT' => 'V',
  'TAA' => 'W', 'TAC' => 'X', 'TAG' => 'Y', 'TAT' => 'Z',
  'TCA' => '1', 'TCC' => '2', 'TCG' => '3', 'TCT' => '4',
  'TGA' => '5', 'TGC' => '6', 'TGG' => '7', 'TGT' => '8',
  'TTA' => '9', 'TTC' => '0', 'TTG' => ' ', 'TTT' => '.'
}

def dnacode_decode(sequence)
  codons = sequence.scan(/.{3}/)
  decoded_sequence = codons.map { |codon| CODON_TABLE[codon] }.join('')
  decoded_sequence
end

def vigenere_decode(cipher, key)
  result = ''
  key_index = 0
  cipher.each_char do |char|
    if char.match(/[A-Za-z]/)
      key_char = key[key_index].upcase
      offset = key_char.ord - 'A'.ord
      result += shiftchar(char, -offset)
      key_index = (key_index + 1) % key.length
    else
      result += char
    end
  end
  result
end

def shiftchar(char, offset)
  if char.match(/[A-Za-z]/)
    base = char.downcase == char ? 'a' : 'A'
    (((char.ord - base.ord + offset) % 26) + base.ord).chr
  else
    char
  end
end

def hex_xor(str, key)
  str.scan(/../).collect { |hex_byte| (hex_byte.to_i(16) ^ key.to_i(16)).chr }.join
end

def xor_decode(hex_message, hex_key)
  hex_xor(hex_message, hex_key)
end


def clear_screen
  Gem.win_platform? ? (system "cls") : (system "clear")
end

def pause_and_clear_screen
  puts "Press any key to continue..."
  gets.chomp
  clear_screen
end

loop do
  clear_screen
  puts "[1] Decoder for Base(32,64,85)"
  puts "[2] Decoder for Number System(Binary, Octal, Decimal, Hexadecimal)"
  puts "[3] Decoder for Rot(n,47)"
  puts "[4] Weiner RSA Decoder"
  puts "[5] Common Modulo RSA Decoder"
  puts "[6] Classic RSA Decoder"
  puts "[7] Chinese Remainder Theorem Decoder"
  puts "[8] Brainfuck Decoder"
  puts "[9] Bacon 26 Decoder"
  puts "[10] Bacon 24 Decoder"
  puts "[11] Morse Code Decoder"
  puts "[12] DNA Code Decoder"
  puts "[13] Vigenere Cipher Decoder"
  puts "[14] XOR Cipher Decoder"
  puts "[15] Exit"
  print "[+] Enter Your Choice:"
  choice = gets.chomp

  case choice
  when '1'
    puts '[=> Decoder for Base(32,64,85)]'
    print '[+] Enter The Cipher:'
    cipher = gets.chomp
    puts '[+] Choose The Encoding Type:'
    puts '[1] Base32'
    puts '[2] Base64'
    puts '[3] Base85'
    enc_type = gets.chomp

    case enc_type
    when '1'
      result = b32_decode(cipher)
      puts "[+] Decoded Text: #{result}"
      pause_and_clear_screen
    when '2'
      result = b64_decode(cipher)
      puts "[+] Decoded Text: #{result}"
      pause_and_clear_screen
    when '3'
      result = b85_decode(cipher)
      puts "[+] Decoded Text: #{result}"
      pause_and_clear_screen
    else
      puts '[+] Invalid Choice. Exiting...'
      pause_and_clear_screen
    end

  when '2'
    puts '[=> Decoder for Number System(Binary, Octal, Decimal, Hexadecimal)]'
    print '[+] Enter The Cipher:'
    cipher = gets.chomp
    puts '[+] Choose The Number System:'
    puts '[1] Binary'
    puts '[2] Octal'
    puts '[3] Decimal'
    puts '[4] Hexadecimal'
    num_type = gets.chomp

    case num_type
    when '1'
      result = bitotext(cipher)
      puts "[+] Decoded Text: #{result}"
      pause_and_clear_screen
    when '2'
      result = octtotext(cipher)
      puts "[+] Decoded Text: #{result}"
      pause_and_clear_screen
    when '3'
      result = dectotext(cipher)
      puts "[+] Decoded Text: #{result}"
      pause_and_clear_screen
    when '4'
      result = hextotext(cipher)
      puts "[+] Decoded Text: #{result}"
      pause_and_clear_screen
    else
      puts '[+] Invalid Choice. Exiting...'
      pause_and_clear_screen
    end

  when '3'
    puts '[=> Decoder for Rot(n,47)]'
    print '[+] Enter The Cipher:'
    cipher = gets.chomp
    result = rot47_decode(cipher)
    puts "[+] Decoded Text: #{result}"
    pause_and_clear_screen

  when '4'
    puts '[=> Weiner RSA Decoder]'
    print "Enter the RSA modulus (n): "
    n = gets.chomp.to_i
    print "Enter the RSA public exponent (e): "
    e = gets.chomp.to_i
    print "Enter the ciphertext (c): "
    c = gets.chomp.to_i
    result = weinerrsa_decode(n, e, c)
    puts "[+] Decoded Text: #{result}"
    pause_and_clear_screen

  when '5'
    puts '[=> Common Modulo RSA Decoder]'
    print '[+] Enter The Values (n1, n2, n3, c):'
    n1_value, n2_value, n3_value, c_value = gets.split.map(&:to_i)
    result = commonmodulus_decode(n1_value, n2_value, n3_value, c_value)
    puts "[+] Decoded Text: #{result}"
    pause_and_clear_screen

  when '6'
    puts '[=> Classic RSA Decoder]'
    print '[+] Enter The Values (n, e, c):'
    n_value, e_value, c_value = gets.split.map(&:to_i)
    result = classicrsa_decode(n_value, e_value, c_value)
    pause_and_clear_screen

  when '7' 
    puts '[=> Chinese Remainder Theorem (CRT) Decoder]'
    print '[+] Enter The Values (p, q, d, c): '
    p_value, q_value, d_value, c_value = gets.split.map(&:to_i)
    result = chinese_remainder_theorem([p_value, q_value], [d_value, c_value])
    puts "[+] Decoded Text: #{result}"
    pause_and_clear_screen

  when '8' 
    puts '[=> Brainfuck Decoder]'
    print '[+] Enter The Brainfuck Code:'
    code = gets.chomp
    result = brainfuck_decode(code)
    puts "[+] Decoded Text: #{result}"
    pause_and_clear_screen

  when '9'
    puts '[=> Bacon 26 Decoder]'
    print '[+] Enter The Bacon 26 Code:'
    code = gets.chomp
    result = bacon26_decode(code)
    puts "[+] Decoded Text: #{result}"
    pause_and_clear_screen

  when '10'
    puts '[=> Bacon 24 Decoder]'
    print '[+] Enter The Bacon 24 Code:'
    code = gets.chomp
    result = bacon24_decode(code)
    puts "[+] Decoded Text: #{result}"
    pause_and_clear_screen

  when '11'
    puts '[=> Morse Code Decoder]'
    print '[+] Enter The Morse Code:'
    code = gets.chomp
    result = morsecode_decode(code)
    puts "[+] Decoded Text: #{result}"
    pause_and_clear_screen

  when '12'
    puts '[=> DNA Code Decoder]'
    print '[+] Enter The DNA Code:'
    code = gets.chomp
    result = dnacode_decode(code)
    puts "[+] Decoded Text: #{result}"
    pause_and_clear_screen

  when '13'
    puts '[=> Vigenere Cipher Decoder]' 
    print '[+] Enter The Cipher:'
    cipher = gets.chomp
    print '[+] Enter The Key:'
    key = gets.chomp
    result = vigenere_decode(cipher, key)
    puts "[+] Decoded Text: #{result}"
    pause_and_clear_screen

  when '14'
    puts '[=> XOR Cipher Decoder //cipher/key in hex,result in text]'
    print '[+] Enter The Cipher:'
    cipher = gets.chomp
    print '[+] Enter The Key:'
    key = gets.chomp
    result = xor_decode(cipher, key)
    puts "[+] Decoded Text: #{result}"
    pause_and_clear_screen

  when '15'
    puts 'Exiting...'
    break

  else
    puts '[+] Invalid Choice. Exiting...'
    break
  end
end
