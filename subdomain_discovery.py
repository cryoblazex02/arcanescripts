import requests
import argparse
import concurrent.futures
from tqdm import tqdm

def discover_subdomains(url, wordlist_path):
    with open(wordlist_path, 'r') as wordlist_file:
        subdomains = wordlist_file.read().splitlines()

    discovered_subdomains = []
    with tqdm(total=len(subdomains), desc="Checking Subdomains", unit="subdomain") as pbar:
        with concurrent.futures.ThreadPoolExecutor() as executor:
            futures = [executor.submit(check_subdomain, url, subdomain, pbar) for subdomain in subdomains]
            for future in concurrent.futures.as_completed(futures):
                discovered_subdomains.extend(future.result())
                pbar.update(1)

    return discovered_subdomains

def check_subdomain(url, subdomain, pbar):
    full_url = f'http://{subdomain}.{url}'
    try:
        response = requests.get(full_url)
        if response.status_code < 400:
            print(f"Discovered subdomain: {full_url}")
            return [full_url]
    except requests.RequestException:
        pass
    return []

def main():
    parser = argparse.ArgumentParser(description='Subdomain discovery using a wordlist')
    parser.add_argument('-u', '--url', help='Target URL', required=True)
    parser.add_argument('-w', '--wordlist', help='Path to the wordlist file', required=True)
    args = parser.parse_args()

    target_url = args.url
    wordlist_path = args.wordlist

    discovered_subdomains = discover_subdomains(target_url, wordlist_path)

    print(f'\nDiscovered subdomains for {target_url}:')
    for subdomain in discovered_subdomains:
        print(subdomain)

if __name__ == '__main__':
    main()
